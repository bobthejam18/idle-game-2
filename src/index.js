// Let Statements
let money = 0;
let autoClickerCost = 5;
let autoClickers = 0;
let autoMoneyGenerationInterval = setInterval(autoMoneyGeneration, 1000);

// Money Button
function moneyButtonClick() {
	money += 1;

	document.getElementById("money").innerHTML = money;
};

// Purchase AutoClicker Button
function purchaseAutoClickerButtonClick() {
	if (money >= autoClickerCost) {
		money -= autoClickerCost;
		autoClickers += 1;
		autoClickerCost += 5 * autoClickers;

		document.getElementById("money").innerHTML = money;
		document.getElementById("autoClickers").innerHTML = autoClickers;
		document.getElementById("autoClickerCost").innerHTML = autoClickerCost;

		clearInterval(autoClickerInterval);

        if (autoClickers > 0) {
			autoMoneyGenerationInterval = setInterval(autoMoneyGeneration, 1000 / autoClickers);
        } else {
			autoMoneyGenerationInterval = setInterval(autoMoneyGeneration, 1000);
        }
	}
};

// Auto Money Generation
function autoMoneyGeneration() {
	if (autoClickers >= 1) {
		money += 1;
		
		document.getElementById("money").innerHTML = money;
		document.getElementById("autoClickers").innerHTML = autoClickers;
	}
};
